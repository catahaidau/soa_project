//
//  RemoteService.h
//  NotesApp
//
//  Created by Catalin Haidau on 01/02/2017.
//  Copyright © 2017 Catalin Haidau. All rights reserved.
//

#import "BaseService.h"
#import <Foundation/Foundation.h>

@interface RemoteService : BaseService

+ (instancetype)sharedService;

- (void)fetchAllNotesWithCompletion:(void (^)(NSArray <Note *> *notes, NSError *error))completion;
- (void)updateNote:(Note *)note completion:(void (^)(Note *note, NSError *error, NSInteger statusCode))completion;
- (void)deleteNote:(Note *)note completion:(void (^)(NSError *, NSInteger))completion;

@end
