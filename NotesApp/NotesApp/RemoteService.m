//
//  RemoteService.m
//  NotesApp
//
//  Created by Catalin Haidau on 01/02/2017.
//  Copyright © 2017 Catalin Haidau. All rights reserved.
//

#import "Note.h"
#import "RemoteService.h"

static NSString * const kConnectionString = @"http://localhost:3000";

@implementation RemoteService

#pragma mark - Setup

+ (instancetype)sharedService {
    static RemoteService *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [self new];
    });
    
    return manager;
}

#pragma mark - REST API

- (void)fetchAllNotesWithCompletion:(void (^)(NSArray<Note *> *, NSError *))completion {
    [self sendRequestWithBaseURLString:kConnectionString
                              endpoint:@"note"
                         requestMethod:GET
                                params:nil
                           requestBody:nil
                            completion:^(id result, NSError *error, NSInteger statusCode) {
                                if (error != nil) {
                                    completion(nil, error);
                                    return;
                                }
                                NSArray *notes = [Note notesArrayFromDictionaryArray:result];
                                completion(notes, nil);
                            }];
}

- (void)updateNote:(Note *)note completion:(void (^)(Note *, NSError *, NSInteger))completion {
    [self sendRequestWithBaseURLString:kConnectionString
                              endpoint:[NSString stringWithFormat:@"note/%@", note.noteId]
                         requestMethod:PUT
                                params:nil
                           requestBody:[note toData]
                            completion:^(id result, NSError *error, NSInteger statusCode) {
                                if (error != nil) {
                                    completion(nil, error, statusCode);
                                    return;
                                }
                                
                                if (statusCode == 200 || statusCode == 409) {
                                    completion([Note instantiateWithDictionary:result], nil, statusCode);
                                } else {
                                    completion(nil, nil, statusCode);
                                }
                            }];
}

- (void)deleteNote:(Note *)note completion:(void (^)(NSError *, NSInteger))completion {
    [self sendRequestWithBaseURLString:kConnectionString
                              endpoint:[NSString stringWithFormat:@"note/%@", note.noteId]
                         requestMethod:DELETE
                                params:nil
                           requestBody:[note toData]
                            completion:^(id result, NSError *error, NSInteger statusCode) {
                                completion(error, statusCode);
                            }];
}

@end
