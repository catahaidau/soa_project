//
//  AppDelegate.h
//  NotesApp
//
//  Created by Catalin Haidau on 01/02/2017.
//  Copyright © 2017 Catalin Haidau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
@end
