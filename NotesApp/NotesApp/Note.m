//
//  Note.m
//  NotesApp
//
//  Created by Catalin Haidau on 01/02/2017.
//  Copyright © 2017 Catalin Haidau. All rights reserved.
//

#import "Note.h"

@implementation Note

+ (instancetype)instantiateWithDictionary:(NSDictionary *)dict {
    Note *note = [self new];
    
    note.noteId = dict[@"id"];
    note.text = dict[@"text"];
    note.updated = dict[@"updated"];
    
    return note;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super init];
    if (self) {
        self.noteId = [coder decodeObjectForKey:@"id"];
        self.text = [coder decodeObjectForKey:@"text"];
        self.updated = [coder decodeObjectForKey:@"updated"];
    }
    return self;
}

- (NSDictionary *)toDictionary {
    NSDictionary *dictionary = @{
                                 @"id":self.noteId,
                                 @"text":self.text,
                                 @"updated":self.updated,
                                 };
    
    return dictionary;
}

+ (NSArray *)notesArrayFromDictionaryArray:(NSArray *)dictionaryArray {
    NSMutableArray *array = [NSMutableArray array];
    
    for (NSDictionary *dictionary in dictionaryArray) {
        [array addObject:[Note instantiateWithDictionary:dictionary]];
    }
    
    return array;
}

+ (NSArray *)dictionaryArrayFromNotesArray:(NSArray *)NotesArray {
    NSMutableArray *array = [NSMutableArray array];
    
    for (Note *note in NotesArray) {
        [array addObject:[note toDictionary]];
    }
    
    return array;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.noteId forKey:@"id"];
    [encoder encodeObject:self.text forKey:@"text"];
    [encoder encodeObject:self.updated forKey:@"updated"];
}

- (NSData *)toData {
    return [NSJSONSerialization dataWithJSONObject:[self toDictionary]
                                           options:NSJSONWritingPrettyPrinted
                                             error:nil];
}

@end
