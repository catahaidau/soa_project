//
//  UIViewController+ErrorHandling.h
//  NotesApp
//
//  Created by Catalin Haidau on 10/06/2018.
//  Copyright © 2018 Catalin Haidau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ErrorHandling)

- (void)showError:(NSError *)error message:(NSString *)message;

@end
