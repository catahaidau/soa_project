//
//  TableViewCell.m
//  NotesApp
//
//  Created by Catalin Haidau on 10/06/2018.
//  Copyright © 2018 Catalin Haidau. All rights reserved.
//

#import "TableViewCell.h"

@interface TableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@end

@implementation TableViewCell

+ (NSString *)reuseIdentifier {
    return @"tableViewCell";
}

- (void)populateWithNote:(Note *)note {
    self.contentLabel.text = note.text;
}

@end
