//
//  Note.h
//  NotesApp
//
//  Created by Catalin Haidau on 01/02/2017.
//  Copyright © 2017 Catalin Haidau. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Note : NSObject <NSCoding>

@property (strong, nonatomic) NSString *noteId;
@property (strong, nonatomic) NSString *text;
@property (nonatomic) NSNumber *updated;

+ (instancetype)instantiateWithDictionary:(NSDictionary *)dict;
+ (NSArray *)notesArrayFromDictionaryArray:(NSArray *)dictionaryArray;
+ (NSArray *)dictionaryArrayFromNotesArray:(NSArray *)NotesArray;

- (NSDictionary *)toDictionary;
- (NSData *)toData;

@end
