//
//  TableViewCell.h
//  NotesApp
//
//  Created by Catalin Haidau on 10/06/2018.
//  Copyright © 2018 Catalin Haidau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note.h"

@interface TableViewCell : UITableViewCell

+ (NSString *)reuseIdentifier;
- (void)populateWithNote:(Note *)note;

@end
